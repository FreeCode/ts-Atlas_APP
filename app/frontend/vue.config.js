const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')

module.exports = {
    productionSourceMap: false,
    configureWebpack: config => {
        config.plugins.push(
            new CompressionWebpackPlugin({
		minRatio: 0.75,
		threshold: 5120,
		deleteOriginalAssets: false,
                filename: '[path].gz[query]',
                algorithm: 'gzip',
                test: new RegExp(
                    '\\.(' + ['html','js','css'].join('|') + ')$'
                ),
            })
        )
        config.externals = {
            d3: "d3",
        }
    }
}
