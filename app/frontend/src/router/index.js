/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/

import Vue from 'vue'
import VueRouter from 'vue-router'
// import VisGraph from "../containers/vis_graph.vue"
// import VisScalars from "../components/vis_scalars.vue"
import BasicTaskInfo from "../components/basic_task_info.vue"
// import TrainingLog from "../components/training_log.vue"
// import VisImages from "../components/vis_images.vue"
// import Task from "../containers/task.vue"
import Dataset from "../containers/dataset.vue"
import Model from "../containers/model.vue"
import Admin from "../containers/admin.vue"
import SignIn from "../containers/signin.vue"
import SignUp from "../containers/signup.vue"
import ShareLink from "../containers/share_link.vue"
// import MyTasks from "../components/my_tasks.vue"
// import CreateTask from "../components/create_task.vue"
// import TaskDetail from "../containers/task_detail.vue"
import UploadDataset from "../components/upload_dataset.vue"
import MyDatasets from "../components/my_datasets.vue"
import PublicDatasets from "../components/public_datasets.vue"
import UploadModel from "../components/upload_model.vue"
import MyModels from "../components/my_models.vue"
import PublicModels from "../components/public_models.vue"
import ManageServer from "../components/manage_server.vue"
import ManageAlgorithm from "../components/manage_algorithm.vue"
import ManageMetrics from "../components/manage.metrics.vue"
import UpdateModel from "../components/update_model.vue"
import UpdateDataset from "../components/update_dataset.vue"
import Sync from '../components/sync.vue'
import ModelZoo from '../containers/modelzoo.vue'
// import VisFlowGraph from "../components/vis_flowChart.vue"
// import VisTexts from "../components/vis_texts.vue"

const Task = () => import(/* webpackChunkName: "task" */ '../containers/task.vue')
const CreateTask = () => import(/* webpackChunkName: "task" */ '../components/create_task.vue')
const MyTasks = () => import(/* webpackChunkName: "task" */ '../components/my_tasks.vue')
const TaskDetail = () => import(/* webpackChunkName: "task" */ '../containers/task_detail.vue')
const TrainingLog = () => import('../components/training_log.vue')

const VisGraph = () => import(/* webpackChunkName: "vis" */ '../containers/vis_graph.vue')
const VisScalars = () => import(/* webpackChunkName: "vis" */ '../components/vis_scalars.vue')
const VisImages = () => import(/* webpackChunkName: "vis" */ '../components/vis_images.vue')
const VisFlowGraph = () => import(/* webpackChunkName: "vis" */ '../components/vis_flowChart.vue')
const VisTexts = () => import(/* webpackChunkName: "vis" */ '../components/vis_texts.vue')


Vue.use(VueRouter)
const routes = [
  { path: '/', name: 'Welcome', redirect: { name: 'Visualize Graph' } },
  {
    path: '/login/:token?',
    name: 'Sync',
    component: Sync,
  },
  {
    path: '/visgraph',
    name: 'Visualize Graph',
    component: VisGraph,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/modelzoo',
    name: 'Model Zoo',
    component: ModelZoo,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/task',
    name: 'Task',
    component: Task,
    redirect: { name: 'Create Task' },
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: 'create_task',
        name: 'Create Task',
        component: CreateTask
      },
      {
        path: 'my_tasks',
        name: 'My Tasks',
        component: MyTasks
      }
    ]
  },
  {
    path: '/task_detail/:task_id',
    name: 'Task Detail',
    component: TaskDetail,
    redirect: { name: 'Basic Task Info' },
    meta: {
      requiresAuth: true
    },
    children: [
      { path: 'basic_task_info', name: "Basic Task Info", component: BasicTaskInfo },
      { path: 'training_log', name: "Training Log", component: TrainingLog },
      { path: 'vis_scalars', name: "Vis Scalars", component: VisScalars },
      { path: 'vis_images', name: "Vis Images", component: VisImages },
      { path: 'vis_texts', name: "Vis Texts", component: VisTexts },
      { path: 'vis_graph', name: "Vis FlowGraph", component: VisFlowGraph }
    ]
  },
  {
    path: '/model',
    name: 'Model',
    component: Model,
    redirect: { name: 'Upload Model' },
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: 'upload_model',
        name: 'Upload Model',
        component: UploadModel
      },
      {
        path: 'my_models',
        name: 'My Models',
        component: MyModels
      },
      {
        path: 'public_models',
        name: 'Public Models',
        component: PublicModels
      },
      {
        path: '/update_model/:model_id/:model_name/:task/:dataset_name/:is_public/:backbone',
        name: 'Update_Model',
        component: UpdateModel,
      },
    ]
  },
  {
    path: '/share/:file_type/:share_link',
    name: 'Share Link',
    component: ShareLink,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/dataset',
    name: 'Dataset',
    component: Dataset,
    redirect: { name: 'Upload Dataset' },
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: 'upload_dataset',
        name: 'Upload Dataset',
        component: UploadDataset
      },
      {
        path: 'my_datasets',
        name: 'My Datasets',
        component: MyDatasets
      },
      {
        path: 'public_datasets',
        name: 'Public Datasets',
        component: PublicDatasets
      },
      {
        path: '/update_dataset/:dataset_id/:is_public/:dataset_name',
        name: 'Update_Dataset',
        component: UpdateDataset,
      },
    ]
  },
  {
    path: '/signin',
    name: 'Sign In',
    component: SignIn,
    meta: {
      requiresNotLogged: true
    }
  },
  {
    path: '/signup',
    name: 'Sign Up',
    component: SignUp,
    meta: {
      requiresNotLogged: true
    }
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin,
    redirect: { name: 'ManageServer' },
    meta: {
      requiresStaff: true
    },
    children: [
      {
        path: 'manage_server',
        name: 'ManageServer',
        component: ManageServer
      },
      {
        path: 'manage_algorithm',
        name: 'ManageAlgorithm',
        component: ManageAlgorithm
      },
      {
        path: 'manage.metrics',
        name: 'ManageMetrics',
        component: ManageMetrics
      }
    ]
  }
]

const router = new VueRouter({
  routes
})



export default router