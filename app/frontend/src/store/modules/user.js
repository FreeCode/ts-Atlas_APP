/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/


import { ROOT_URL, DUBHE_URL } from '../../config'
import axios from 'axios'
import router from '../../router'
import Vue from 'vue'
import { setIsDialogOpen, setDialogContent } from '../utils'

// initial state
const state = {
    errors: "",
    is_dialog_open: false,
    dialog_content: "",
}

// getters
const getters = {
}

let emailRegex = new RegExp("^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(.[a-zA-Z0-9_-]+)+$");
let errorUNRegex = new RegExp("username$");
let errorEMRegex = new RegExp("email$");

// actions
const actions = {
    signUp({ dispatch, commit, state, }, user_info) {
        console.log(user_info)
        if (user_info.username == '') {
            commit("setIsDialogOpen", true)
            commit("setDialogContent", "用户名不能为空")
        }
        else if (user_info.email == '') {
            commit("setIsDialogOpen", true)
            commit("setDialogContent", "邮箱不能为空")
        }
        else if (!(emailRegex.test(user_info.email))) {
            console.log('signup email')
            commit("setIsDialogOpen", true)
            commit("setDialogContent", "邮箱格式不正确")
        }
        else if (user_info.password == '') {
            commit("setIsDialogOpen", true)
            commit("setDialogContent", "密码不能为空")
        }
        else if (user_info.password2 == '') {
            commit("setIsDialogOpen", true)
            commit("setDialogContent", "请再次输入密码")
        }
        else if (user_info.password != user_info.password2) {
            console.log('signup password')
            commit("setIsDialogOpen", true)
            commit("setDialogContent", "两次密码不一致")
        }
        else {
            axios.post(`${ROOT_URL}/api/user/user`, {
                user_info
            }).then((response) => {
                console.log("signup success", response.data)
                // router.push('welcome')
                dispatch('signIn', { 'username': user_info.username, 'password': user_info.password })
            }).catch((errors) => {
                console.log("signup error", errors)
                commit('setErrors', errors)
                commit('setIsDialogOpen', true)
                commit('setDialogContent', '注册失败')
            })
        }
    },
    signIn({ commit, state }, user_info) {
        console.log(user_info)
        axios.post(`${ROOT_URL}/api/user/token_auth`, {
            'username': user_info.username,
            'password': user_info.password,
        }).then((response) => {
            console.log("signin success", response.data)
            commit('updateAuth', response.data.auth, { root: true })
        }).catch((errors) => {
            console.log("signin error", errors)
            commit('setErrors', errors)
            commit('setIsDialogOpen', true)
            commit('setDialogContent', '登录失败')
        })
    },
    syncUser({ dispatch, commit, state }, token) {
        console.log(token)
        commit("removeAuth", false, { root: true })
        axios.put(`${ROOT_URL}/api/user/user`, {
            token
        }).then((response) => {
            console.log("sync: success", response.data)
            // commit('updateAuth', response.data.auth, { root: true })
            let username = response.data.username
            let password = response.data.password
            dispatch('signIn', { 'username': username, 'password': password })
        }).catch((errors) => {
            console.log("sync: signup error", errors)
            commit('setErrors', errors)
            commit('setIsDialogOpen', true)
            commit('setDialogContent', errors)
        })
    }
}

const mutations = {
    setErrors(state, errors) {
        state.errors = errors
    },
    setIsDialogOpen, setDialogContent
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}