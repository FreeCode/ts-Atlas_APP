/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/


import { ROOT_URL } from '../../config'
import axios from 'axios'
import { parseTime, setIsDialogOpen, setDialogContent, convertArray2String } from '../utils'

// initial state
const state = {
    task_details: {},
    options: {},
    errors: "",
    is_dialog_open: false,
    dialog_content: "",
    vis_data: [],
    stage:[],
    remain_stage:[],
    finished_stage:[],
    log_data: [],
    algorithms: {}
}

// getters
const getters = {
}

// actions
const actions = {
    getTaskDetails({ commit, state }, task_id) {
        console.log('get task details', task_id)
        axios.get(`${ROOT_URL}/api/task/task_details`, {
            params: {
                task_id: task_id
            }
        })
            .then((response) => {
                console.log("task_details", response.data);
                let task_details = response.data;
                task_details.created_time = parseTime(task_details.created_time);
                task_details.started_time = parseTime(task_details.started_time);
                task_details.completed_time = parseTime(task_details.completed_time);
                if (task_details.tasks) {
                    task_details.tasks = task_details.tasks.reduce(convertArray2String);
                }
                if (task_details.teacher_models) {
                    task_details.teacher_models = task_details.teacher_models.reduce(convertArray2String);
                }
                if (task_details.datasets) {
                    task_details.datasets = task_details.datasets.reduce(convertArray2String);
                }
                if (task_details.student_models) {
                    task_details.student_models = task_details.student_models.reduce(convertArray2String);
                }
   
                task_details.algorithms.fields = task_details.algorithms.fields.filter( f=>(f.field_value!='') )

                console.log('algorithms', task_details.algorithms)
                if (task_details.algorithms){
                    console.log('set algorithms')
                    commit('setAlgorithms', task_details.algorithms)
                }
                //task_details.log = [["2019-04-23T18:25:43.511Z", 0, "ahahahahah"],["2019-04-23T18:25:43.511Z", 1, "ahahahahah"],["2019-04-23T18:25:43.511Z", 2, "ahahahahah"]]
                console.log('traininglog', task_details.log)
                if (task_details.log){
                    console.log('set training log')
                    commit('setTrainingLog', task_details.log)
                }
                console.log('visdata', task_details.vis_data)
                if (task_details.vis_data){
                    console.log('set vis data')
                    commit('setVisData', task_details.vis_data)
                }
                if (task_details.stage){
                    console.log('set stage')
                    commit('setStage', task_details.stage)
                    let remain_stage = task_details.stage.filter(function (step) {
                        return !step.complete
                    })
                    let finished_stage = task_details.stage.filter(function (step) {
                        return step.complete
                    })
                    commit("setRemainStage",remain_stage)
                    commit("setFinishedStage",finished_stage)

                }
                commit('setTaskDetails', task_details)
            })
            .catch((errors) => {
                console.log("error", errors)
                commit('setTaskDetails', {})
                commit('setErrors', errors)
                commit("setIsDialogOpen", true)
                commit("setDialogContent", "任务不存在或无权限")
            })
    },
    deleteReorgTasks({ dispatch, commit, state }, task_ids) {
        console.log('tasks', task_ids)
        axios.delete(`${ROOT_URL}/api/task/reorg_tasks`, {
            params: { task_ids: task_ids }
        })
            .then((response) => {
                console.log('delete success')
                commit("setIsDialogOpen", true)
                commit("setDialogContent", "删除成功")
                // dispatch('getTaskDetails', task_ids[0])
            })
            .catch((errors) => {
                console.log('delete failure')
                commit('setErrors', errors)
                commit("setIsDialogOpen", true)
                commit("setDialogContent", "删除失败")
                // dispatch('getTaskDetails', task_ids[0])
            })
    },
}

const mutations = {
    setTaskDetails(state, task_details) {
        state.task_details = task_details
    },
    setErrors(state, errors) {
        state.errors = errors
    },
    setAlgorithms(state, algorithms) {
        state.algorithms = algorithms
    },
    setTrainingLog(state, log) {
        state.log_data = log
    },
    setVisData(state, vis_data){
        state.vis_data = vis_data;
    },
    setStage(state,stage){
        state.stage = stage;
    },
    setRemainStage(state,remain_stage){
        state.remain_stage = remain_stage
    },
    setFinishedStage(state,finished_stage){
        state.finished_stage = finished_stage
    },
    setIsDialogOpen, setDialogContent
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}