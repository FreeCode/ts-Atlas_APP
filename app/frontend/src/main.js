/*  Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './custom.scss'
import 'font-awesome/css/font-awesome.css'
import router from './router'
import store from './store'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  console.log(to.matched)
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.is_loggedin) {
      next({ name: 'Sign In' })
    } else {
      next()
    }
  }
  else if (to.matched.some(record => record.meta.requiresNotLogged)) {
    if (store.getters.is_loggedin) {
      next({ name: 'Welcome' })
    } else {
      next()
    }
  }
  else if (to.matched.some(record => record.meta.requiresStaff)) {
    if (!store.getters.is_staff) {
      next({ name: 'Welcome' })
    } else {
      next()
    }
  }
  else {
    next()
  }
})

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
