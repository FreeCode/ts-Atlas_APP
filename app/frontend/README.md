# frontend
<!-- ## 功能要求
![](Atlas前端.png)

- 蓝色部分：未完成
- 绿色部分：界面已完成，部分请求逻辑完成
- 红色部分：下一步准备完成 -->

## 前端工具
- vue（包括vuex状态管理，vue-router官方路由）
- bootstrap-vue（基于bootstrap，移除了与vue冲突的部分）
- d3

## 文件结构
```
frontend/src/
    - components/ 基本组件
    - containers/ 包含多个组件的面板
        - vis_graph.vue
        - upload_model.vue
        - ...
    - router/ 注册所有路由
    - store/
        - modules/ 模块的状态与方法，统一使用命名空间
            - visualization.js
            - upload.js
            - ...
        - index.js 模块注册，用户验证信息（当前用户名、token等）
    - tests/ 用于测试接口的假数据
        - json-server.json 服务器配置
        - sample_route.json 路由配置
        - sample.json 假数据
    - config.js 设置全局变量，如后端接口的ROOT_URL
    - custom.scss 设置基础样式
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


<!-- ## 前端测试
使用json-server工具，运行方式：

本地安装json-server:

`npm install -g json-server`

cd到测试文件所在目录：

`cd frontend/src/tests/ #注意相对路径`

开启json-server:

`json-server -w sample.json -r sample_route.json`

运行成功之后，所有前端请求都会获取到来自：localhost:8000的返回 -->

<!-- ## 注意
npm安装新的包时，使用`--save`选项以保存依赖到`package.json` -->