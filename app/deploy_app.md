# 部署
**仅测试了nginx 1.16.0，node 13.6.0，uwsgi 2.0.18环境**

[nginux ubuntu 安装步骤](https://nginx.org/en/linux_packages.html#Ubuntu)

## 前端
### 步骤
0. 进入`frontend/`文件夹

1. babel.config.js中设置生产环境中取消所有控制台输出  
`src/config.js`中设置后端url

2. `npm run build` 得到`dist`文件夹，包括js文件与html  

3. nginx在`/etc/nginx/nginx.conf`中设置server
```
server
{
    listen 9000;#监听端口
    server_name localhost;#域名
    root /mnt/atlas/Atlas/app/frontend/dist;#上一步中得到的dist文件路径
    location / {
        index index.html;
    }
}
```

4. 启动nginx或重启nginx：`nginx -s reload`

5. 通过 `ip:port/` 访问  
如`http://10.214.211.207:9000/`

<!-- ### 注意
1. 阿里云上除了80端口外可能需要开放端口权限，而且很慢

2. 部署本身只需第二步中得到的文件和nginx即可 -->

## 后端
0. 进入`backend/`文件夹

1. 修改`uwsgi.ini`中路径以及添加其他设置

2. `uwsgi --ini uwsgi.ini`启动uwsgi服务  
需要设置home为虚拟环境，或**使用conda环境**  
阿里云上需设置lib路径：  
`export LD_LIBRARY_PATH=/mnt/atlas/anaconda3/envs/atlas/lib:$LD_LIBRARY_PATH`

或者`uwsgi --reload backend.pid`重启uwsgi服务

3. nginx在`/etc/nginx/nginx.conf`中设置
```
location /api { # api设置
    include uwsgi_params;
    uwsgi_pass unix:///mnt/atlas/Atlas/app/backend/backend.sock; #uwsgi.ini中对应sock文件
}
location /file { # 处理静态文件
    alias /mnt/atlas/Atlas/app/backend/file;
}
```

4. 在http选项中设置`client_max_body_size 1024m;`以便上传大文件 

5. 重启nginx：`nginx -s reload`

## 数据库

0. 安装MySQL 5.7

1. 新建用户：用户名为atlas，密码为atlas123

2. 新建数据库`atlas`，授予用户权限

3. 将数据解压在后端`backend`根目录

```
- backend/
    - setting.py：  
    ...
- file/
- media/
- db_initial_data.json
```

4. 使用django导入`db_initial_data.json`
