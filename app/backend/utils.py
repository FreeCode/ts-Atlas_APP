"""
/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/
"""

from functools import wraps
import logging, sys

def getClientIp(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def logRequest(logger_name):
    logger = logging.getLogger(logger_name)
    def logDecorator(func):
        # "ip username method url status_code"
        @wraps(func)
        def withLogging(*args, **kwargs):
            logger.info("{} {} {} {}".format(
                getClientIp(args[1]), args[1].user.username, args[1].method, args[1].get_full_path()))
            res = func(*args, **kwargs)
            logger.info(res.status_code)
            return res
        return withLogging
    return logDecorator

