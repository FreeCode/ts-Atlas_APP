import uuid

from django.core.files.storage import FileSystemStorage
import os


class DatasetStorage(FileSystemStorage):
    from django.conf import settings

    def __init__(self, location=settings.MEDIA_ROOT, base_url=settings.MEDIA_URL):
        super(DatasetStorage, self).__init__(location, base_url)

    def _open(self, name, mode='rb'):
        pass

    def _save(self, name, content):
        ext = os.path.splitext(name)[1]
        d = os.path.dirname(name)
        fn = uuid.uuid1().hex
        name = os.path.join(d, fn + ext)
        return super(DatasetStorage, self)._save(name, content)
