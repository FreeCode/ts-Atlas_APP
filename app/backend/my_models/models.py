"""
/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/
"""
from django.db import models

# Create your models here.
from my_models.storage import ModelStorage
from user.models import User
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

class Models(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    model_name = models.CharField(max_length=255, unique=True)
    dataset_name = models.CharField(max_length=255)
    md5 = models.CharField(max_length=256)
    task = models.CharField(max_length=256)
    # to be remove
    backbone = models.CharField(max_length=256) 
    url = models.URLField()
    file = models.FileField(upload_to='model', storage=ModelStorage())
    is_public = models.BooleanField(default=False)
    created_time = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()

    class Meta:
        db_table = "models"
        ordering = ['-id']

    def __str__(self):
        return self.id


@receiver(pre_delete, sender=Models)
def model_delete(sender, instance, **kwargs):
    instance.file.delete(False)


class Share_Link(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    file_id = models.IntegerField()
    url_str = models.CharField(max_length=50)
    file_type = models.CharField(max_length=1, default='m')
    expired_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    objects = models.Manager()

    class Meta:
        db_table = "share_link"
        ordering = ['-id']

    def __str__(self):
        return self.id


class Share_Model(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    file_id = models.IntegerField()
    file_type = models.CharField(max_length=1, default='m')
    expired_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    objects = models.Manager()

    class Meta:
        db_table = "share_model"
        ordering = ['-id']

    def __str__(self):
        return self.id
