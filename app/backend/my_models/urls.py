"""
/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/
"""
from django.urls import path, re_path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from . import views

urlpatterns = [
    path('models', views.Upload.as_view()),
    path('public_models', views.PublicModels.as_view()),
    # path('download_model', views.DownloadModel.as_view()),
    path('share_link', views.ShareLink.as_view()),
    path('share_model', views.ShareModel.as_view()),
    path('upload_options', views.upload_options.as_view(), name='upload_options'),
    path('upload_model_to_user', views.UploadModelToUser.as_view(), name='upload_model_to_user'),
]
