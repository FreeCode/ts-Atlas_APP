"""
/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/
"""
import hashlib
import json
import os
from datetime import timedelta
from functools import partial
from urllib.parse import urljoin
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from utils import logRequest

# Create your views here.
from django.views import View
from my_models.models import Models, Share_Link, Share_Model
from task.models import TASK_OPTIONS
from task.models import Task
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.utils import jwt_decode_handler
from django.http import JsonResponse, HttpResponse, Http404, FileResponse, HttpResponseForbidden
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import parser_classes
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.parsers import FileUploadParser, MultiPartParser, JSONParser
import re
import uuid
from backend import settings
from user.models import User
import logging
from django.db.models import F

logger_name = 'my_models.views'
logger = logging.getLogger(logger_name)

class upload_options(APIView):
    @logRequest(logger_name)
    def get(self, request, format=None):
        options = {}
        for name, option_contents in TASK_OPTIONS.items():
            options[name] = [ {"id":k, "name":v}  for k, v in option_contents.items()]
        return Response(options, status=status.HTTP_200_OK)


class Upload(APIView):
    permission_classes = (IsAuthenticated,)

    @logRequest(logger_name)
    def post(self, request):
        my_model = Models()
        my_model.user = request.user
        file_info = request.data["file_info"]
        info = json.loads(file_info)
        my_model.file = request.data["file"]
        my_model.is_public = info["is_public"]
        my_model.model_name = info["model_name"]
        my_model.dataset_name = info["dataset_name"]
        my_model.task = info["task"]
        my_model.backbone = info["backbone"]
        my_model.md5 = get_md5(my_model.file)
        # if Models.objects.filter(md5=my_model.md5) is not None:
        #     return Response(data="duplicate file", status=status.HTTP_400_BAD_REQUEST)
        if not my_model.file:
            return Response(data="no file for upload", status=status.HTTP_400_BAD_REQUEST)
        # (filename, extension) = os.path.splitext(my_model.model_name)
        # if extension not in['.pth', '.pkl']:
        #    return Response(data="wrong file for upload", status=status.HTTP_400_BAD_REQUEST)
        try:
            my_model.save()
            url = urljoin('file/', my_model.file.name)
            my_model.url = url
            my_model.save()
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        return Response(data="success upload", status=status.HTTP_200_OK)

    @logRequest(logger_name)
    def get(self, request):
        user_info = request.user
        model = Models.objects.filter(user=user_info).values(
            "id", "model_name", "dataset_name", "task", "backbone", "is_public", "url", "created_time"
        )
        share_model = Share_Model.objects.filter(user=user_info).filter(file_type='m').values("file_id")
        share_model_list = list(share_model)
        file_id_list = []
        for v in share_model_list:
            file_id_list.append(v["file_id"])
        model_share = Models.objects.filter(id__in=file_id_list).values(
                "id", "model_name", "dataset_name", "task", "backbone", "is_public", "url", "created_time"
                )
        model_share_list = list(model_share)
        model_list = list(model)
        for v in model_list:
            v["is_possessed"] = True
        for v in model_share_list:
            v["is_possessed"] = False
        model_list.extend(model_share_list)
        for v in model_list:
            v["task"] = v["task"].lstrip("[").rstrip("]").replace("'","").split(', ')
            v["task"] = [TASK_OPTIONS["tasks"][t] for t in v["task"]]
        return Response(model_list, status=status.HTTP_200_OK)

    def delete(self, request):
        model_ids = [int(v) for v in request.query_params.getlist('model_ids[]', [])]
        try:
            Models.objects.filter(id__in=model_ids).filter(user=request.user).delete()
            return Response(data="success", status=status.HTTP_200_OK)
        except:
            return Response(data="error", status=status.HTTP_400_BAD_REQUEST)

    @logRequest(logger_name)
    def put(self, request):
        user = request.user
        file_info = request.data["file_info"]
        info = json.loads(file_info)
        model_id = info["model_id"]
        my_model = Models.objects.filter(id=model_id).first()
        if my_model.user != user:
            return Response(data="wrong user", status=status.HTTP_400_BAD_REQUEST)
        my_model.is_public = info["is_public"]
        my_model.model_name = info["model_name"]
        my_model.dataset_name = info["dataset_name"]
        my_model.task = info["task"]
        my_model.backbone = info["backbone"]
        try:
            my_model.save()
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        return Response(data="success", status=status.HTTP_200_OK)


class PublicModels(APIView):
    @logRequest(logger_name)
    def get(self, request):
        try:
            models = Models.objects.filter(is_public=True).values("id", "model_name", "dataset_name", "task", "backbone", "url", "created_time", username=F("user__username"))
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        for v in models:
            v["task"] = v["task"].lstrip("[").rstrip("]").replace("'","").split(', ')
            v["task"] = [TASK_OPTIONS["tasks"][t] for t in v["task"]]
        logger.debug(models)
        return Response(models, status=status.HTTP_200_OK)

# class DownloadModel(APIView):

#     permission_classes = (IsAuthenticated,)

#     def post(self, request):
#         file_id = request.POST.file_id
#         file_name = request.POST.file_name
#         file_type = request.POST.file_type
#         if file_type == 'm':
#             file = Models.objects.get(id=file_id)
#             if request.user == file.user or Share_Model.objects.filter(file_id=file_id).filter(user=request.user):
#                 response = FileResponse(open(file_name, 'rb'))
#                 response['content_type'] = "application/octet-stream"
#                 response['Contentontent-Disposition'] = 'attachment; filename=' + os.path.basename(file_name)
#                 return response
#             else:
#                 return Response(data="You don't have permission", status=status.HTTP_400_BAD_REQUEST)
#         elif file_type == 'd':
#             file = Dataset.objects.get(id=file_id)
#             if request.user == file.user or Share_Model.objects.filter(file_id=file_id).filter(user=request.user):
#                 response = FileResponse(open(file_name, 'rb'))
#                 response['content_type'] = "application/octet-stream"
#                 response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_name)
#                 return response
#             else:
#                 return Response(data="You don't have permission", status=status.HTTP_400_BAD_REQUEST)


class ShareLink(APIView):
    permission_classes = (IsAuthenticated,)

    @logRequest(logger_name)
    def post(self, request):
        link = Share_Link()
        link.file_id = request.data["file_id"]
        model = Models.objects.filter(id=link.file_id).filter(user=request.user)
        if len(model) == 0:
            return Response(data='No such file', status=status.HTTP_400_BAD_REQUEST)
        link.file_type = request.data["file_type"]
        duration = request.data["duration"]
        link.expired_time = timezone.now()+timedelta(days=int(duration))
        fn = uuid.uuid1().hex
        link.url_str = fn
        link.save()
        return Response(data=fn, status=status.HTTP_200_OK)


class ShareModel(APIView):
    permission_classes = (IsAuthenticated,)

    @logRequest(logger_name)
    def post(self, request):
        url_str = request.data["share_link"]
        link = Share_Link.objects.filter(url_str=url_str, file_type="m").first()
        if link is None:
            return Response(data="invalid link", status=status.HTTP_400_BAD_REQUEST)
        time_now = timezone.now()
        if time_now > link.expired_time:
            return Response(data="over time", status=status.HTTP_400_BAD_REQUEST)
        share_model = Share_Model.objects.filter(file_id=link.file_id).filter(file_type=link.file_type).filter(user=request.user)
        if len(share_model) > 0:
            return Response(data="already exist", status=status.HTTP_400_BAD_REQUEST)
        model = Models.objects.filter(id=link.file_id).first()
        if(model.user == request.user):
            return Response(data="You are the owner of model", status=status.HTTP_400_BAD_REQUEST)
        model = Share_Model()
        model.file_id = link.file_id
        model.file_type = link.file_type
        model.user = request.user
        model.expired_time = time_now + timedelta(days=7)
        model.save()
        return Response(data="obtain permission", status=status.HTTP_200_OK)


class UploadModelToUser(APIView):
    permission_classes = (AllowAny,)

    @logRequest(logger_name)
    def post(self, request):
        my_model = Models()
        file_info = request.data["file_info"]
        info = json.loads(file_info)
        task_uid = info["task_uid"]
        task = Task.objects.filter(task_uid=task_uid).first()
        if task is None:
            return Response(data="task does not exist", status=status.HTTP_400_BAD_REQUEST)
        server_name = info["server_name"]
        pwd = info["pwd"]
        server = task.server
        if server is None:
            return Response(data="task is not start", status=status.HTTP_400_BAD_REQUEST)
        if server.username != server_name:
            return Response(data="You are not involved in the task", status=status.HTTP_400_BAD_REQUEST)
        if server.check_password(pwd) is False:
            return Response(data="wrong password", status=status.HTTP_400_BAD_REQUEST)
        my_model.user = task.user
        my_model.file = request.data["file"]
        my_model.is_public = info["is_public"]
        my_model.model_name = info["model_name"]
        my_model.dataset_name = info["dataset_name"]
        my_model.task = info["task"]
        my_model.backbone = info["backbone"]
        my_model.md5 = get_md5(my_model.file)
        # if Models.objects.filter(md5=my_model.md5) is not None:
        #     return Response(data="duplicate file", status=status.HTTP_400_BAD_REQUEST)
        if not my_model.file:
            return Response(data="no file for upload", status=status.HTTP_400_BAD_REQUEST)
        # (filename, extension) = os.path.splitext(my_model.model_name)
        # if extension not in['.pth', '.pkl']:
        #    return Response(data="wrong file for upload", status=status.HTTP_400_BAD_REQUEST)
        try:
            my_model.save()
            url = urljoin('file/', my_model.file.name)
            my_model.url = url
            my_model.save()
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        # return Response(data="success upload", status=status.HTTP_200_OK)
        return Response(data=my_model.url, status=status.HTTP_200_OK)
    

def get_md5(file, block_size=65536):
    md5 = hashlib.md5()
    for buf in iter(partial(file.read, block_size), b''):
        md5.update(buf)
    return md5.hexdigest()
