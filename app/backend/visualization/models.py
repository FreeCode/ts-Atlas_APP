"""
/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/
"""
from django.db import models
from my_models.storage import ModelStorage

# Create your models here.
class AtlasCollection(models.Model):
    atlas_uid = models.CharField(max_length = 32, unique=True, default=0)
    # user = models.ForeignKey('user.User', on_delete = models.CASCADE, related_name = 'atlas')
    url = models.URLField()
    file = models.FileField(upload_to='graph', storage=ModelStorage())
    description = models.CharField(max_length = 512)
    md5 = models.CharField(max_length=256)

    def __str__(self):
        return 'Graph:  {}'.format( self.atlas_uid)