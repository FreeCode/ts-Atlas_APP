"""
/** Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* =============================================================
*/
"""
from django.test import TestCase

# Create your tests here.
from server.models import Server
from server.utils import signInServer
class ServerTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up data for the whole TestCase
        cls.username = 'rerorero'
        cls.password = 'kaqyin'
        cls.server = Server.objects.create(username=cls.username, password=cls.password)

    def testSignIn(self):
        res = signInServer(ServerTests.username, ServerTests.password)
        self.assertEqual(res, True)

    def testSignIn2(self):
        res = signInServer(ServerTests.username, 'qtaro')
        self.assertEqual(res, False)

    def testSignIn3(self):
        res = signInServer('anothername', 'qtaro')
        self.assertEqual(res, False)
