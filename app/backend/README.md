# 后端

## 工具
Django  3.0.2
Django REST framework 3.11.0

数据库：~~Django自带的sqlite3~~ MySQL 5.7  
在`setting.py`中切换
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'atlas',
        'USER': 'atlas',
        'PASSWORD': 'atlas',
        'OPTIONS': {
            "unix_socket": "/var/run/mysqld/mysqld.sock",
        },
    }
}
```
其余依赖见requirements.txt

## 文件结构
- backend/ 后端总体配置
    - setting.py：  
        通过installed_app进行应用的注册，如user的注册  
        中间件corsheaders用于自动完成请求的token验证和更新  
        REST_framework的allowany部分：为了开发时测试方便，暂停token的验证
    - urls.py：注册子组件中的api地址
    - ...
- user/ 用户注册、登录模块
    - models.py：用户table的相关属性和方法
    - views.py：处理对应的请求
    - urls.py：用户子组件内部的路由，将请求地址和views.py中的方法关联
    - ...
- visualization/ 可视化模块
- utils.py 日志函数装饰器

## 运行
进入`/app/backend/`目录

1. 安装依赖  
`pip install -r requirements.txt`

2. 建立数据库表  
```
python manage.py makemigrations
python manage.py migrate
```

3. 开启后端服务  
`python manage.py runserver 10.214.211.207:8000`

<!-- ## 开发
以`visualization`为例

1. 创建名为`visualization`的子组件：  
`python manage.py startapp visualization`

2. 在`backend/apps.py`和`backend/urls.py`下注册该组件：  
```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    'user.apps.UserConfig',
    'visualization.apps.VisualizationConfig',
]
```

```
urlpatterns = [
    # path('admin/', admin.site.urls),
    path('api/user/', include('user.urls')),
]
```

3. 进入`visualization`文件夹  
`models.py`内创建数据库表，操作数据库  
`urls.py`将请求的url和处理方法对应  
`views.py`内处理请求  
`tests.py`创建测试用例

4. 开发测试  
创建对应测试用例后，`python manage.py test`即可  
注意，数据库用户需要有权限创建`test_atlas`数据库

5. 初始化数据
`python manage.py loaddata <your json>`  
保存当前数据库内容`python manage.py dumpdata <app name>`  

## Task交互
通过mysql中的atlas数据库中的task_task表格与后端进行交互  
表格定义见`backend/task/models.py`  
- task与result字段长度暂定为1024
- 日期为python中的`datetime.datetime`实例，未处理的任务该属性为`None`

示例见`backend/task/view.py` -->

