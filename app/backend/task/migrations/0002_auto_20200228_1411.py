# Generated by Django 3.0.1 on 2020-02-28 06:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='task_type',
            field=models.CharField(choices=[('PT', 'Plain Train'), ('KR', 'Knowledge Reorganization')], default='KR', max_length=2),
        ),
        migrations.CreateModel(
            name='AtlasCollection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('atlas_uid', models.CharField(default=0, max_length=32, unique=True)),
                ('url', models.CharField(max_length=512)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='atlas', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
