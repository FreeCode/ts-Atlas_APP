# Generated by Django 3.0.2 on 2020-07-01 04:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0005_auto_20200228_2054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='task_type',
            field=models.CharField(choices=[('PT', 'Plain Train'), ('KA', 'Knowledge Amalgamation')], default='KA', max_length=2),
        ),
    ]
