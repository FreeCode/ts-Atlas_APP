# API文档
- [API文档](#api文档)
  - [前端需求](#前端需求)
    - [用户](#用户)
      - [从天枢同步](#从天枢同步)
      - [注册](#注册)
      - [登录](#登录)
    - [图可视化](#图可视化)
      - [获取度量方法](#获取度量方法)
      - [获取度量对应的图](#获取度量对应的图)
    - [任务](#任务)
      - [获取新任务选项](#获取新任务选项)
      - [获取我的任务](#获取我的任务)
      - [新建任务](#新建任务)
      - [删除任务](#删除任务)
      - [获取任务训练过程信息](#获取任务训练过程信息)
      - [算力机检查任务是否被删除](#算力机检查任务是否被删除)
    - [模型](#模型)
      - [获取上传选项](#获取上传选项)
      - [上传新模型](#上传新模型)
      - [获取已上传模型](#获取已上传模型)
      - [删除已上传模型](#删除已上传模型)
      - [创建分享链接](#创建分享链接)
      - [获取分享的权限](#获取分享的权限)
    - [数据集](#数据集)
      - [上传新数据集](#上传新数据集)
      - [获取当前数据集](#获取当前数据集)
      - [删除已上传数据集](#删除已上传数据集)
      - [创建分享链接](#创建分享链接-1)
      - [获取分享的权限](#获取分享的权限-1)
    - [管理算力机](#管理算力机)
      - [获取当前算力机](#获取当前算力机)
      - [新建算力机](#新建算力机)
      - [删除已有算力机](#删除已有算力机)
    - [管理算法字段](#管理算法字段)
      - [获取当前算法](#获取当前算法)
      - [新建算法](#新建算法)
      - [更新算法](#更新算法)
      - [删除已有算法](#删除已有算法)
    - [管理度量](#管理度量)
      - [获取所有度量信息](#获取所有度量信息)
      - [新建度量](#新建度量)
      - [更新度量](#更新度量)
      - [删除已有度量](#删除已有度量)
  - [后端开发进展](#后端开发进展)
 
## 前端需求
API设计参考RESTfull风格，响应使用状态码（2xx成功，4xx请求错误，5xx服务器错误）

headers: 除用户模块外，所有api都需token验证

```
{
    'Authorization': 'JWT {your token}',
}
```

### 用户

#### 从天枢同步
请求：

`PUT /api/user/user`

数据：  
```
{
    token
}
```

响应： 

成功则返回用户名和密码，用于登录atlas

若失败应返回失败原因：用户名or邮箱不唯一等等  

#### 注册
请求：

`POST /api/user/user`

数据：  
```
{
    user_info: {
        username: "",
        email: "",
        password: "",
    }
}
```

响应：  
若失败应返回失败原因：用户名or邮箱不唯一等等  

#### 登录

请求：

`POST /api/user/token_auth`

数据：

```
username: "",
password: "",
```

响应：  
若成功返回token  
若失败应返回失败原因：用户不存在，密码错误等等  

### 图可视化
#### 获取度量方法
请求：  
`GET /api/visualization/metric_names`  
响应：  
```
['Metric1', 'Metric1', ...]
```

#### 获取度量对应的图
请求：  
`GET /api/visualization/graph`  
参数：  
```
metric_names: ["Metric1", "Metric1"]
```
响应：  TODO: update
```
{
    "Metric1": {
        "nodes": [
            {
                "id": "inception_v3_imagenet_classification",
                "acc": 0.77316,
                "num_parameters": 27161264
            },
            ...
        ],
        "edges": [
            {
                "source": "inception_v3_imagenet_classification",
                "target": "dpn131_imagenet_classification",
                "weight": 57.48996353149414
            },
            ...
        ]
    },
    "Metric2":{
        ...
    }
}
```

### 任务
元数据定义见`backend/task/models.py`

#### 获取新任务选项
请求：  
`GET /api/task/reorg_task_options`

响应：
```
{
    "models": [{
        "id": 0,
        "name": "xxxmodel",
    }],
    "datasets": [{
        "id": 0,
        "name": "xxxmodel",
    }],
    "algorithms": [ //新增的算法信息
        {
            "id":   0
            "alg_name": "Knowledge Distillation",
            "fields": [
                {"field_name": "model size", "field_value": "0.5", "field_note": "target size"},
                {"field_name": "optimizer", "field_value": "sgd", "field_note": "ahahah"},
                {"field_name": "lr", "field_value": "0.1", "field_note": "learning rate"},
            ]
        }
        {
            "id":   1
            "alg_name": "Layer wise Amalgamation",
            "fields": [
                {"field_name": "optimizer", "field_value": "sgd", "field_note": "ahahah"},
                {"field_name": "lr", "field_value": "0.1", "field_note": "learning rate"},
            ]
        }
    ],
}
```


#### 获取我的任务

请求：  
`GET /api/task/reorg_tasks`

TODO: 核对前端

响应：
```
[
    {
        "id": 1,
        "task_type": "Knowledge Reorganization",
        "tasks": ["segmentation", "depth"],
        "datasets": ["ds_name"],
        "teacher_models": ["teacher1", "teacher2"],
        "student_models": ["student1", "student2"],
        "algorithms": [
            {
                "id": id,
                "alg_name": "sbm",
                "fields": [{
                    "id": id,
                    "field_name": xx,
                    "field_value": xx,
                    "field_note": xx,
                }]
            }
        ],
        "created_time": "2012-04-23T18:25:43.511Z",
        "started_time": null,
        "completed_time": null,
        "progress": 80.8,
        "url": ""
    },
]
```

#### 新建任务
请求：
`POST /api/task/reorg_tasks`
TODO: 核对前端
数据：
```
{
    task_info: {
        tasks: ['seg', 'dep'],
        datasets: [ds_ids],
        teacher_models: [ids],
        student_models: [ids],
        algorithms: [
            {
                "id": id,
                "alg_name": "sbm",
                "fields": [{
                    "id": id,
                    "field_name": xx,
                    "field_value": xx,
                    "field_note": xx,
                }]
            }
        ]
        size: 100,
    }
}
```

响应：成功或对应失败原因

#### 删除任务
请求：
`DELETE /api/task/reorg_tasks`

参数：
```
task_ids: [],
```

响应：成功或对应失败原因


#### 获取任务训练过程信息
请求：  
`GET /api/task/task_details`

参数：
```
task_id: task_id,
```

响应：在[获取我的任务](#获取我的任务)的基础上添加。
1. `title`不能重复；
2. `data_type`为`scalar, image, graph, text`中的一种；
3. `data`数组中当类型为`graph`时，参考[获取度量对应的图](#获取度量对应的图)(TODO: update)；为其它类型时，元素为`[time, step, value]`三元数组：
   1. `time`为UTC时间字符串，时区为0；
   2. `step`为整数
   3. `value`根据`data_type`不同而不同：
      1. `scalar`种类：为浮点数
      2. `image`种类：为url字符
      3. `text`种类：字符串
```
vis_data: [
    {
        "title": "loss",
        "data_type": "scalar",
        "data": [
            ["2019-04-23T18:25:43.511Z", 1, 13.4],
            ["2019-04-23T18:25:43.511Z", 2, 14.5],
            ["2019-04-23T18:25:43.511Z", 3, -3],
            ["2019-04-23T18:25:43.511Z", 50, 18.45]
        ]
    }
]
```

#### 算力机检查任务是否被删除

请求：
`GET /api/task/is_task_existed`

参数：
```
task_uid: xx,
server_name: server,
server_password: server,
```

成功响应：True or False

失败响应：Invalid server name，Invalid server password等等

### 模型
TODO: 去除backbones
#### 获取上传选项 
请求：  
`GET /api/model/upload_options`

响应：  
success: 
```
{
    "tasks": [
        {
            "id": "seg",
            "name": "segmentation"
        },
        {
            "id": "cla",
            "name": "classification"
        },
        {
            "id": "det",
            "name": "detection"
        }
    ]
    "backbones": [
        {
            "id": "0",
            "name": "resnet"
        },
        {
            "id": "1",
            "name": "segnet"
        },
        {
            "id": "2",
            "name": "alexnet"
        }
    ]
}
```

#### 上传新模型
请求：  
`POST /api/model/models`  

header:
`'Content-Type': 'multipart/form-data'`

数据：  
```
{
    file, //binary
    file_info: {
        model_name: "name",
        task: "seg",
        dataset_name: "imagenet",
        backbone: "res",
        is_public: true,
    }
}
```
其中file_info部分数据经过json转字符串处理

#### 获取已上传模型
请求：
`GET /api/model/models`

响应：  
```
[
    {
        "id": 1,
        "model_name": "name",
        "dataset_name": "imagenet",
        "task": ["segmentation", "depth"],
        "backbone": "resnet",
        "is_public": true,
        "url": "download_url",
        "created_time": "2019-04-23T18:25:43.511Z",
        "is_possessed": true
    },
]
```
对于该用户创建的模型，is_possessed为true；对于别人分享给该用户的模型，is_possessed为false

#### 删除已上传模型
请求：  
`DELETE /api/model/models`

参数：  
```
model_ids: [1, 2]
```

响应：  
success or error

#### 创建分享链接
请求：
`POST /api/model/share_link`

数据：
```
file_id: 1, 
file_type: "m", //数据集则为"d",
duration: 7, //单位为天
```

响应：对非公开数据集返回生成的对应字符串`{unique_string}`，否则返回错误

#### 获取分享的权限
请求：
`POST /api/model/share_model`

数据：
```
share_link: "unique_string"
```

响应：成功或失败

### 数据集

#### 上传新数据集
请求：  
`POST /api/dataset/datasets`  

header:
`'Content-Type': 'multipart/form-data'`

数据：  
```
{
    file, //binary
    file_info: {
        dataset_name: "imagenet",
        is_public: true,
    }
}
```
其中file_info部分数据经过json转字符串处理

#### 获取当前数据集
请求：
`GET /api/dataset/datasets`

响应：  
```
[
    {
        "id": 1,
        "dataset_name": "name",
        "is_public": true,
        "url": "download_url",
        "created_time": "2019-04-23T18:25:43.511Z",
        "is_possessed": true
    },
]
```
对于该用户创建的数据集，is_possessed为true；对于别人分享给该用户的数据集，is_possessed为false

#### 删除已上传数据集
请求：  
`DELETE /api/dataset/datasets`

参数：  
```
dataset_ids: [1, 2]
```

响应：  
success or error

#### 创建分享链接
请求：
`POST /api/dataset/share_link`

数据：
```
file_id: 1, 
file_type: "m", //数据集则为"d",
duration: 7, //单位为天
```

响应：对非公开数据集返回生成的对应字符串`{unique_string}`，否则返回错误

#### 获取分享的权限
请求：
`POST /api/dataset/share_dataset`

数据：
```
share_link: "unique_string"
```

响应：成功或失败

### 管理算力机

#### 获取当前算力机
请求：
`GET /api/admin/servers`

响应：  
```
[
    {
        "id": 1,
        "username": "name",
        "last_login": "2019-04-23T18:25:43.511Z"
    },
]
```

#### 新建算力机
请求：  
`POST /api/admin/servers`  
数据：  
```
{
    username: "server1",
    password: "123456",
}
```

#### 删除已有算力机
请求：  
`DELETE /api/admin/servers`

参数：  
```
server_ids: [1, 2]
```

响应：  
success or error

### 管理算法字段

#### 获取当前算法
请求：
`GET /api/task/algorithm_info`

响应：  
```
[
    {
        id: 0,
        alg_name: "xx",
        fields: [
            {
                field_name: "xx",
                field_value: "",
                field_note: "",
            }
        ]
    }
]
```

#### 新建算法
请求：  
`POST /api/task/algorithm_info`  
数据：  
```
{
    alg_name: "xx",
    fields: [
        {
            field_name: "xx",
            field_value: "",
            field_note: "",
        }
    ]
}
```

#### 更新算法
请求：  
`PUT /api/task/algorithm_info`  
数据：  
```
{
    id: 0,
    alg_name: "xx",
    fields: [
        {
            field_name: "xx",
            field_value: "",
            field_note: "",
        }
    ]
}
```

#### 删除已有算法
请求：  
`DELETE /api/task/algorithm_info`

参数：  
```
algorithm_ids: [1, 2]
```

响应：  
success or error

### 管理度量

#### 获取所有度量信息
请求：
`GET /api/visualization/metrics`

响应：  
```
[
    {
        id: 0,
        name: "xx",
        url: "xx",
        description: "xx"
    }
]
```

#### 新建度量
请求：  
`POST /api/visualization/metrics`  
数据：  
```
{
    file: //binary
    metric_info: 
        {
            name: "xx",
            description: ""
        }
}
```

#### 更新度量
请求：  
`PUT  /api/visualization/metrics`  
数据：  
```
{
    file: //binary
    metric_info: 
        {
            id: 0,
            name: "xx",
            description: ""
        }
}
```

#### 删除已有度量
请求：  
`DELETE /api/visualization/metrics`

参数：  
```
metric_id: 1
```

响应：  
success or error

## 后端开发进展

- [ ] `POST /api/model/models`  
1. md5是用来判断用户是否上传了相同的文件，如果和已有的文件相同，就直接把url指向原来那个一样的文件，而不是重复存储，现在还没有实现。【优先级低】
