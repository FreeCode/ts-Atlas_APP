# Copyright 2020 Zhejiang Lab and Zhejiang University. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =============================================================

from .ade20k import ADE20K
from .caltech import Caltech101, Caltech256
from .camvid import CamVid
from .cityscapes import Cityscapes
from .cub200 import CUB200
from .fgvc_aircraft import FGVCAircraft
from .nyu import NYUv2
from .stanford_cars import StanfordCars
from .stanford_dogs import StanfordDogs
from .sunrgbd import SunRGBD
from .voc import VOCClassification, VOCSegmentation
from .dataset import LabelConcatDataset

from torchvision import datasets as torchvision_datasets

from .unlabeled import UnlabeledDataset